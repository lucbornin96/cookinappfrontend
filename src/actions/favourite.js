import api from "../api";

export const getAllFavourite = (token) => (dispatch) =>
  api.favourite.getAllFavourite(token);

export const putAsFavourite = (recipe_id) => (dispatch) => {
  return api.favourite.putAsFavourite({
    recipe_id: recipe_id,
    jwt: localStorage.TOKEN_USER_AUTH.toString(),
  });
};

export const deleteFromFavourite = (recipe_id) => (dispatch) => {
  return api.favourite.deleteFromFavourite({
    jwt: localStorage.TOKEN_USER_AUTH,
    recipe_id: recipe_id,
  });
};
export const getMyFavourite = () => (dispatch) => {
  return api.favourite.getMyFavourite();
};
