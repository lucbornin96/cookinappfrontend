import axios from "axios";

var serveurAcces = "http://51.195.45.145:8080/";
var jwt = localStorage.TOKEN_USER_AUTH;

export default {
  user: {
    login: (credentials) => {
      console.log(credentials);
      return axios
        .post(serveurAcces + "api/account/login", credentials)
        .then((res) => res.data);
    },
    signup: (user) =>
      axios
        .post(serveurAcces + "api/account/create", user)
        .then((res) => res.data),
    deleteAccount: (credentials) =>
      axios
        .post(serveurAcces + "api/account/delete", credentials)
        .then((res) => res.data),
  },
  recipe: {
    createRecipe: (newRecipe) =>
      axios
        .post(serveurAcces + "api/recipe", newRecipe, {
          headers: { jwt: jwt },
        })
        .then((res) => res.data),
    getRecipeid: (recipe_id, jwt) =>
      axios
        .get(serveurAcces + "api/recipe/" + recipe_id, {
          headers: { jwt: jwt },
        })
        .then((res) => res.data),
    // TODO: requete find recipe
    getRecipe: (findRecipe) => {
      return axios
        .get(serveurAcces + "api/recipe?" + findRecipe, {
          headers: { jwt: jwt },
        })
        .then((res) => {
          return res.data;
        });
    },
  },
  food: {
    getFoodName: (foodName) =>
      axios
        .get(serveurAcces + "api/food?name=" + foodName, {
          headers: { jwt: jwt },
        })
        .then((res) => {
          console.log(res.data);
          return res.data;
        }),
    getFoodInfo: (foodId) =>
      axios
        .get(serveurAcces + "api/food/get?food_id=" + foodId)
        .then((res) => res.data),
    getFoodUnit: (foodId) =>
      axios
        .get(serveurAcces + "api/food/" + foodId + "/units", {
          headers: { jwt: jwt },
        })
        .then((res) => res.data),
  },
  favourite: {
    getMyFavourite: (token) =>
      axios
        .get(serveurAcces + "api/recipe/favourite", {
          headers: { jwt: jwt },
        })
        .then((res) => res.data),
    putAsFavourite: (addFavourite) =>
      axios
        .put(
          serveurAcces + "api/recipe/favourite/" + addFavourite.recipe_id,
          {},
          {
            headers: { jwt: jwt },
          }
        )
        .then((res) => res.data),
    deleteFromFavourite: (deleteFavourite) => {
      return axios
        .delete(
          serveurAcces + "api/recipe/favourite/" + deleteFavourite.recipe_id,
          { headers: { jwt: jwt } }
        )
        .then((res) => {
          return res.data;
        });
    },
  },
  shopping_list: {
    getMyShopping_list: (token) =>
      axios
        .get(serveurAcces + "api/recipe/favourite", {
          headers: { jwt: jwt },
        })
        .then((res) => res.data),
    putAsFavourite: (addFavourite) =>
      axios
        .put(
          serveurAcces + "api/recipe/favourite/" + addFavourite.recipe_id,
          {},
          {
            headers: { jwt: jwt },
          }
        )
        .then((res) => res.data),
    deleteFromFavourite: (deleteFavourite) => {
      return axios
        .delete(
          serveurAcces + "api/recipe/favourite/" + deleteFavourite.recipe_id,
          { headers: { jwt: jwt } }
        )
        .then((res) => {
          return res.data;
        });
    },
  },
};
