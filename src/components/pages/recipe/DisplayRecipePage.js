import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Grid } from "semantic-ui-react";
import { getRecipeid } from "../../../actions/recipes";
import {
  putAsFavourite,
  deleteFromFavourite,
} from "../../../actions/favourite";

let imageRecipe;
class DisplayRecipePage extends React.Component {
  state = {
    recipe: null,
    data: {
      token: localStorage.TOKEN_USER_AUTH,
      recipe_id: 2,
    },
    image: null,
  };

  componentDidMount() {
    this.props
      .getRecipeid(
        Number(this.props.match.params.recipe_id),
        localStorage.TOKEN_USER_AUTH
      )
      .then((recipe) => {
        this.setState({ recipe: recipe });
      });
  }

  onSubmit = () => {
    if (!this.state.recipe.is_favourite) {
      this.props.putAsFavourite(this.state.recipe.recipe_id).then((success) => {
        this.setState((state) => {
          state.recipe.is_favourite = true;
          return state;
        });
      });
    } else {
      this.props
        .deleteFromFavourite(this.state.recipe.recipe_id)
        .then((success) => {
          this.setState((state) => {
            state.recipe.is_favourite = false;
            return state;
          });
        });
    }
  };

  render() {
    return (
      <Container>
        {this.state.recipe && (
          <Grid>
            <Grid.Row>
              <h2>{this.state.recipe.title}</h2>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column width={5}>{imageRecipe}</Grid.Column>

              <Grid.Column width={5}>
                {!this.state.recipe.is_favourite ? (
                  <button className="ui green button" onClick={this.onSubmit}>
                    Add to favourite
                  </button>
                ) : (
                  <button className="ui orange button" onClick={this.onSubmit}>
                    Delete From favourite
                  </button>
                )}
              </Grid.Column>
            </Grid.Row>

            <h3>Préparation</h3>
            {this.state.recipe.instructions.map((instruction) => {
              var unique_id = this.state.recipe.recipe_id + instruction;
              return (
                <Grid.Row key={unique_id}>
                  <li>{instruction}</li>
                </Grid.Row>
              );
            })}
            <h3>Description :</h3>
            {this.state.recipe.description}
            <h3>nutri_score_label :</h3>
            {this.state.recipe.nutri_score_label}
            <h3>picture base 64 :</h3>
            <img src={`${this.state.recipe.base_64_picture}`} />
          </Grid>
        )}
      </Container>
    );
  }
}

DisplayRecipePage.propTypes = {
  getRecipeid: PropTypes.func.isRequired,
  putAsFavourite: PropTypes.func.isRequired,
  deleteFromFavourite: PropTypes.func.isRequired,
  // recipe_id: PropTypes.number.isRequired,
};

export default connect(null, {
  getRecipeid,
  putAsFavourite,
  deleteFromFavourite,
})(DisplayRecipePage);
