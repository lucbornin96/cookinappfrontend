import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import DeleteForm from "../forms/DeleteForm";
import { deleteAccount } from "../../actions/auth";

class DeleteAccountPage extends React.Component {
  submit = data =>
    this.props.deleteAccount(data).then(() => this.props.history.push("/dashboard"));

  render() {
    return (
      <div>
        <h1>Deleta my account page</h1>

        <DeleteForm submit={this.submit} />
      </div>
    );
  }
}

DeleteAccountPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  deleteAccount: PropTypes.func.isRequired
};

export default connect(null, { deleteAccount })(DeleteAccountPage);
