import React from "react";
import { Link } from "react-router-dom";
import { Button, Grid, List, Form, Checkbox } from "semantic-ui-react";
import SearchRecipeForm from "../forms/SearchRecipeForm";
import SearchIngredientFrom from "../forms/SearchIngredientFrom";
import imagesTest from "./test.jpg";

// TODO : metre le SearchRecipeForm dans la topNav
// TODO :afficher les recettes de bases
class DashboardPage extends React.Component {
  state = {
    recipe: [],
    data: {
      tagsIngredient: [],
      pescetarian: null,
      vegetarian: null,
      vegan: null,
      gluten_free: null,
    },
    month: [
      {
        key: 1,
        value: "January",
        text: "January",
      },
      {
        key: 2,
        value: "February",
        text: "February",
      },
    ],
  };
  // FIXME: ajouter 2 tags, en supprimer 1 et la racherche ne ce refait pas
  onIngredientSelect = (food) => {
    this.setState((state) => {
      state.data.tagsIngredient = [...state.data.tagsIngredient, food];
      return state;
    });
  };

  RemoveIngredient = (ingredientId) => () => {
    this.setState({
      data: {
        ...this.state.data,
        tagsIngredient: this.state.data.tagsIngredient
          .filter((item) => item.food_id !== ingredientId)
          .map((item) => {
            if (item.food_id > ingredientId) {
              return { food_id: item.food_id - 1, name: item.name };
            } else {
              return item;
            }
          }),
      },
    });
  };

  onRecipeSearch = (recipe) => {
    this.setState({ recipe: recipe });
  };

  render() {
    const { recipe, data } = this.state;
    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column width={5}>
              <h1>My favorite recipes</h1>
            </Grid.Column>

            <Grid.Column width={5}>
              <Button primary as={Link} to="/createRecipe">
                Create a new recipe
              </Button>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={5}>
              <SearchRecipeForm
                ingredients={this.state.data.tagsIngredient}
                onRecipeSearch={this.onRecipeSearch}
              />
            </Grid.Column>

            <Grid.Column width={5}>
              <label htmlFor="ingredient">Ingredient :</label>
              <SearchIngredientFrom
                onIngredientSelect={this.onIngredientSelect}
              />
              <List divided verticalAlign="middle">
                {data.tagsIngredient.map((item) => (
                  <List.Item key={item.food_id}>
                    <span
                      className="ui tag label"
                      onClick={this.RemoveIngredient(item.food_id)}
                    >
                      {item.name}
                    </span>
                  </List.Item>
                ))}
              </List>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <List divided>
          {recipe.map((item) => {
            var recipeId = item.recipe_id;
            return (
              <List.Item
                key={item.recipe_id}
                style={{ paddingBottom: "20px", paddingTop: "20px" }}
              >
                <Form.Field>
                  <Grid>
                    <Grid.Column width={3} as={Link} to={"/recipe/" + recipeId}>
                      <img
                        src={imagesTest}
                        style={{ width: "200px" }}
                        alt="description"
                      />
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <h2>
                        <a href={"/recipe/" + item.recipe_id}>{item.title}</a>
                      </h2>
                      {item.instructions.map((instruction) => {
                        var unique_id = item.recipe_id + instruction;
                        return <div key={unique_id}>{instruction}</div>;
                      })}
                    </Grid.Column>
                  </Grid>
                </Form.Field>
              </List.Item>
            );
          })}
        </List>
      </div>
    );
  }
}

export default DashboardPage;
