import React from "react";
import PropTypes from "prop-types";
import { Nav, Navbar, NavDropdown, ButtonGroup, Button } from "react-bootstrap";
import { connect } from "react-redux";
import * as actions from "../../actions/auth";
import logo from "./CookingAppIcon.png";
import "./styleNavbar.css";

class TopNavbar extends React.Component {
  state = {};

  componentDidMount() {
    this.setState({ isAuthenticated: localStorage.TOKEN_USER_AUTH });
  }

  render() {
    const { isAuthenticated } = this.state;
    return (
      <Navbar fixed="top" className="navbar">
        <Navbar.Brand href="/">
          <img
            src={logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="Cooking App logo"
          />
          Cooking App
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto menu">
            {isAuthenticated ? (
              <NavDropdown title="Menu" id="basic-nav-dropdown" alignRight>
                <NavDropdown.Item href="/">Acceuil</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/dashboard">
                  Mon compte
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/profile">Mon profile</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/listeDeCourse">
                  Ma liste de course
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/" onClick={() => this.props.logout()}>
                  Se déconnecter
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <ButtonGroup
                aria-label="Basic example"
                className="btn-groupe-navbar"
              >
                <Button href="/signup" bsPrefix="btn-navbar">
                  S'inscrire
                </Button>
                <Button href="/login" bsPrefix="btn-navbar">
                  Se connecter
                </Button>
              </ButtonGroup>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

TopNavbar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token,
    user: state.user,
  };
}
export default connect(mapStateToProps, { logout: actions.logout })(TopNavbar);
