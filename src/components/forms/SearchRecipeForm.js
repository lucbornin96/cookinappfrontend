import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form, Grid, Button } from "semantic-ui-react";
import { getRecipe } from "../../actions/recipes";

class SearchRecipeForm extends React.Component {
  state = {
    query: "",
    data: {
      recipe: [],
    },
    loading: false,
    reqRecipes: [],
  };

  onSubmit = () => {
    const arrayFood_id = this.props.ingredients.map(
      (ingredient) => ingredient.food_id
    );
    const recipeTitle = this.state.data.recipe;
    const token = null;
    this.props.getRecipe(recipeTitle, token, arrayFood_id).then((recipes) => {
      return this.props.onRecipeSearch(recipes);
    });
  };

  onChangeData = (e) => {
    this.setState({
      data: { ...this.state.data, [e.target.id]: e.target.value },
    });
  };

  componentDidMount = () => {
    this.props.getRecipe("", null, "", "").then((recipes) => {
      return this.props.onRecipeSearch(recipes);
    });
  };

  render() {
    const { data } = this.state;
    return (
      <Grid>
        <Grid.Column>
          <Form>
            <Form.Field>
              <label>recipe title :</label>
              <input
                type="text"
                id="recipe"
                name="inputRecipes"
                placeholder="recipe"
                value={data.recipe}
                onChange={this.onChangeData}
              />
            </Form.Field>
            <Button onClick={this.onSubmit} primary>
              Search recipe
            </Button>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

SearchRecipeForm.propTypes = {
  onRecipeSearch: PropTypes.func.isRequired,
  getRecipe: PropTypes.func.isRequired,
};

export default connect(null, { getRecipe })(SearchRecipeForm);
