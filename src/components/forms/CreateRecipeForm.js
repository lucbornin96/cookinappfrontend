import React from "react";
import PropTypes from "prop-types";
import { List, Grid, Form, Button, Message, Dropdown } from "semantic-ui-react";
import InlineError from "../messages/InlineError";
import SearchIngredientFrom from "../forms/SearchIngredientFrom";
import { getFoodInfo, getFoodUnit } from "../../actions/food";
import { connect } from "react-redux";
import FileBase64 from "react-file-base64";

// TODO: L'utilisateur n'est pas cense pouvoire ecrire dans le dropdown unit
// TODO: 1er index unit ne fonctionne pas
// TODO: ArrayList a refaire
let newIngredient;
class CreateRecipeForm extends React.Component {
  state = {
    data: {
      title: "",
      instructions: [],
      ingredients: [],
      serving: null,
      cookingTime: null,
      base64: [],
    },
    inputInstruction: "",
    inputIngredient: [],
    inputQuantity: "",
    inputUnit: "",
    loading: false,
    errors: {},
    options: [],
    reqUnits: {},
    arrayList: [
      {
        key: 1,
        value: 1,
        text: 1,
      },
      {
        key: 2,
        value: 2,
        text: 2,
      },
      {
        key: 3,
        value: 3,
        text: 3,
      },
    ],
    files: [],
  };

  getFiles(files) {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        base64: files[0].base64,
      },
    });
  }
  upload = () => {
    console.log(this.state.data.base64);
    console.log("data:image/jpeg;base64," + this.state.files);
  };
  handleAddInstruction = () => {
    const errors = this.validateAddInstruction(this.state.inputInstruction);
    this.setState({ errors });
    if (this.state.inputInstruction) {
      this.setState((state) => {
        state.data.instructions = [
          ...state.data.instructions,
          {
            id: this.state.data.instructions.length,
            todo: this.state.inputInstruction,
          },
        ];
        state.inputInstruction = "";
        return state;
      });
    }
  };

  //// TODO: mettre au propre cette fonction il y a un .then qui ne sert a rien
  onIngredientSelect = (food) => {
    this.props
      .getFoodUnit(food.food_id)
      .then((food) => {
        this.setState({ ...this.state.data, inputIngredient: { food } });
        return food;
      })
      .then((foodUnit) => {
        const options = [];
        const unitsHash = {};
        foodUnit.forEach((units) => {
          unitsHash[units.name] = units;
          options.push({
            key: units.unit_id,
            value: units.unit_id,
            text: units.name,
          });
        });
        this.setState({
          loading: false,
          options,
          reqUnits: unitsHash,
          inputIngredient: [food.food_id, food.name],
        });
      });
  };

  handleAddIngredient = () => {
    const errors = this.validateAddIngredient(
      this.state.inputIngredient,
      this.state.inputUnit,
      this.state.inputQuantity
    );
    this.setState({ errors });
    if (
      this.state.inputIngredient &&
      this.state.inputQuantity &&
      this.state.inputUnit
    ) {
      newIngredient = {
        id: this.state.inputIngredient[0],
        label: this.state.inputIngredient[1],
        quantity: this.state.inputQuantity,
        unit: this.state.inputUnit,
      };
      this.setState({
        ...this.state,
        data: {
          ...this.state.data,
          ingredients: [...this.state.data.ingredients, newIngredient],
        },
        inputUnit: "",
        inputQuantity: "",
        options: [],
      });
    }
  };

  handleRemoveInstruction = (instructionId) => () => {
    this.setState({
      data: {
        ...this.state.data,
        instructions: this.state.data.instructions
          .filter((item) => item.id !== instructionId)
          .map((item) => {
            if (item.id > instructionId) {
              return { id: item.id - 1, todo: item.todo };
            } else {
              return item;
            }
          }),
      },
    });
  };
  handleRemoveIngredient = (ingredientId) => () => {
    this.setState({
      data: {
        ...this.state.data,
        ingredients: this.state.data.ingredients
          .filter((item) => item.id !== ingredientId)
          .map((item) => {
            if (item.id > ingredientId) {
              return {
                id: item.id - 1,
                label: item.label,
                quantity: item.quantity,
                unit: item.unit,
              };
            } else {
              return item;
            }
          }),
      },
    });
  };

  onChangeData = (e) =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
    });

  onChange = (e) =>
    this.setState({
      [e.target.name]: e.target.value,
    });

  onSelectUnit = (e, data) => {
    this.setState({
      ...this.state,
      inputUnit: data.value,
    });
  };
  onSelectServing = (e, data) => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        serving: data.value,
      },
    });
  };
  onSelectCookingTime = (e, data) => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        cookingTime: data.value,
      },
    });
  };

  onChangeList = (e) => {
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        instructions: this.state.data.instructions.map((item) => {
          if (item.id === Number([e.target.name])) {
            return { id: item.id, todo: e.target.value };
          } else {
            return item;
          }
        }),
      },
    });
  };

  onSubmit = () => {
    const errors = this.validateSubmit(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props.submit(this.state.data).catch((err) => {
        this.setState({
          errors: { global: err.response.data.error },
          loading: false,
        });
      });
    }
  };

  validateSubmit = (data) => {
    const errors = {};
    if (!data.title) errors.title = "Title can't be blank";
    if (!data.instructions.length)
      errors.instructions = "Instructions can't be blank";
    if (!data.ingredients.length)
      errors.ingredients = "Ingredients can't be blank";
    return errors;
  };

  validateAddInstruction = (inputInstruction) => {
    const errors = {};
    if (!inputInstruction) errors.instruction = "Instruction can't be blank";
    return errors;
  };

  validateAddIngredient = (inputIngredient, inputUnit, inputQuantity) => {
    const errors = {};
    if (!inputIngredient) errors.ingredient = "ingredient can't be blank";
    if (!inputUnit) errors.ingredient = "Unit can't be blank";
    if (!inputQuantity) errors.ingredient = "Quantity can't be blank";
    return errors;
  };

  render() {
    const { data, inputInstruction, inputQuantity, errors, loading } =
      this.state;
    let imagePreview;
    if (this.state.data.base64 !== null) {
      imagePreview = <img src={this.state.data.base64} />;
    }
    return (
      <Form loading={loading}>
        {errors.global && (
          <Message negative>
            <Message.Header>Something went wrong</Message.Header>
            <p>{errors.global}</p>
          </Message>
        )}
        <Form.Field error={!!errors.title}>
          <label htmlFor="title">Title :</label>
          <input
            type="text"
            id="title"
            name="title"
            placeholder="Title"
            value={data.title}
            onChange={this.onChangeData}
          />
          {errors.title && <InlineError text={errors.title} />}
        </Form.Field>

        <Grid>
          <Grid.Row divided>
            <Grid.Column width={8}>
              <Form.Field error={!!errors.instructions}>
                <label htmlFor="instruction">Instruction :</label>
                <input
                  type="text"
                  id="instruction"
                  name="inputInstruction"
                  placeholder="Instruction"
                  value={inputInstruction}
                  onChange={this.onChange}
                />
                {errors.instruction && (
                  <InlineError text={errors.instruction} />
                )}
              </Form.Field>
              <Button icon="plus" onClick={this.handleAddInstruction}></Button>

              <List divided size="huge" verticalAlign="middle">
                {data.instructions.map((item) => (
                  <List.Item key={item.id}>
                    <Form.Field>
                      <label htmlFor="instruction">
                        Instruction n°{item.id}:
                      </label>
                      <Button
                        icon="minus"
                        onClick={this.handleRemoveInstruction(item.id)}
                      ></Button>
                      <input
                        type="text"
                        id={item.id}
                        name={item.id}
                        value={item.todo}
                        onChange={this.onChangeList}
                      />
                    </Form.Field>
                  </List.Item>
                ))}
              </List>
            </Grid.Column>
            <Grid.Column width={8}>
              <Grid.Row>
                <Grid.Column>
                  <Form.Field>
                    <label htmlFor="ingredient">Ingredient :</label>
                    <SearchIngredientFrom
                      onIngredientSelect={this.onIngredientSelect}
                    />
                  </Form.Field>
                </Grid.Column>
                <Grid.Column>
                  <Form.Field>
                    <label htmlFor="quantity">Quantité :</label>
                    <input
                      type="text"
                      id="quantity"
                      name="inputQuantity"
                      placeholder="Quantité"
                      value={inputQuantity}
                      onChange={this.onChange}
                    />
                  </Form.Field>
                </Grid.Column>
                <Grid.Column>
                  <Form.Field>
                    <label htmlFor="unit">Unité :</label>
                    <Dropdown
                      search
                      fluid
                      placeholder="selectionnez une unitée"
                      value={this.state.inputUnit}
                      options={this.state.options}
                      loading={this.state.loading}
                      onChange={this.onSelectUnit}
                    />
                  </Form.Field>
                  <Button
                    icon="plus"
                    onClick={this.handleAddIngredient}
                  ></Button>
                </Grid.Column>
              </Grid.Row>
              {errors.ingredient && <InlineError text={errors.ingredient} />}
              <List divided size="huge" verticalAlign="middle">
                {data.ingredients.map((item) => (
                  <List.Item key={item.id}>
                    <Form.Field>
                      <label htmlFor="ingredients">
                        Ingredient n°{item.id}:
                      </label>
                      <label htmlFor="ingredients">
                        id: {item.id} - value : {item.label} - quantity :{" "}
                        {item.quantity} - unit : {item.unit}
                      </label>
                    </Form.Field>
                    <Button
                      icon="minus"
                      onClick={this.handleRemoveIngredient(item.id)}
                    ></Button>
                  </List.Item>
                ))}
              </List>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={8}>
              {imagePreview}
              <FileBase64 multiple={true} onDone={this.getFiles.bind(this)} />
              <button
                type="button"
                className="btn btn-primary btn-block"
                onClick={this.upload}
              >
                Upload
              </button>
            </Grid.Column>

            <Grid.Column width={8}>
              <Form.Field error={!!errors.serving}>
                <label htmlFor="servingNumber">Serving number :</label>
                <Dropdown
                  search
                  fluid
                  placeholder="Serving number selection"
                  value={this.state.data.serving}
                  options={this.state.arrayList}
                  onChange={this.onSelectServing}
                />
              </Form.Field>

              <Form.Field error={!!errors.cookingTime}>
                <label htmlFor="cookingTime">Cooking time :</label>
                <Dropdown
                  search
                  fluid
                  placeholder="Cooking time selection"
                  value={this.state.data.cookingTime}
                  options={this.state.arrayList}
                  onChange={this.onSelectCookingTime}
                />
              </Form.Field>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Button onClick={this.onSubmit} primary>
          Create a new recipe
        </Button>
      </Form>
    );
  }
}

CreateRecipeForm.propTypes = {
  submit: PropTypes.func.isRequired,
  getFoodInfo: PropTypes.func.isRequired,
  getFoodUnit: PropTypes.func.isRequired,
};

export default connect(null, { getFoodInfo, getFoodUnit })(CreateRecipeForm);
