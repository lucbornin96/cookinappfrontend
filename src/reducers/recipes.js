import { createSelector } from "reselect";
import { RECIPES_FETCHED,RECIPE_CREATE } from "../types";

export default function recipes(state = {}, action = {}) {
  switch (action.type) {
    case RECIPES_FETCHED:
    case RECIPE_CREATE:
      return { ...state, ...action.data.entities.recipes };
    default:
      return state;
  }
}

// SELECTORS

export const recipesSelector = state => state.recipes;

export const allrecipesSelector = createSelector(recipesSelector, recipesHash =>
  Object.values(recipesHash)
);
