//cd directory //npm start
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.min.css";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import { userLoggedIn } from "./actions/auth";
import TopNavbar from "./components/navigation/TopNavbar";

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.TOKEN_USER_AUTH) {
  const user = {
    token: localStorage.TOKEN_USER_AUTH,
  };
  store.dispatch(userLoggedIn(user));
}

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <TopNavbar />
      <Route component={App} />
    </Provider>
  </BrowserRouter>,
  document.getElementById("react")
);

serviceWorker.unregister();
