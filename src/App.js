import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "./components/pages/HomePage";
import LoginPage from "./components/pages/LoginPage";
import SignupPage from "./components/pages/SignupPage";
import DashboardPage from "./components/pages/DashboardPage";
import ProfilePage from "./components/pages/ProfilePage";
import MyAccountPage from "./components/pages/MyAccountPage";
import DeleteAccountPage from "./components/pages/DeleteAccountPage";
import CreateRecipePage from "./components/pages/recipe/CreateRecipePage";
import DisplayRecipePage from "./components/pages/recipe/DisplayRecipePage";
import Shopping_list from "./components/pages/shopping_list/shoppingListPage";
import UserRoute from "./components/routes/UserRoute";
import GuestRoute from "./components/routes/GuestRoute";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ConfirmationPage from "./components/pages/ConfirmationPage";

//location={location} fix un conflit entre react_redux_connect et react_router
const App = ({ location, isAuthenticated }) => (
  <div className="ui container" style={{ marginTop: 90 + "px" }}>
    <Switch>
      <Route
        location={location}
        path="/"
        exact
        component={(props) => <HomePage {...props} />}
      />
      <Route
        location={location}
        path="/confirmation/:token"
        exact
        component={ConfirmationPage}
      />
      <GuestRoute
        location={location}
        path="/login"
        exact
        component={(props) => <LoginPage {...props} />}
      />
      <GuestRoute
        location={location}
        path="/signup"
        exact
        component={(props) => <SignupPage {...props} />}
      />
      <UserRoute
        location={location}
        path="/dashboard"
        exact
        component={(props) => <DashboardPage {...props} />}
      />
      <UserRoute
        location={location}
        path="/profile"
        exact
        component={(props) => <ProfilePage {...props} />}
      />
      <UserRoute
        location={location}
        path="/myaccount"
        exact
        component={(props) => <MyAccountPage {...props} />}
      />
      <UserRoute
        location={location}
        path="/deleteAccount"
        exact
        component={(props) => <DeleteAccountPage {...props} />}
      />
      <UserRoute
        location={location}
        path="/createRecipe"
        exact
        component={(props) => <CreateRecipePage {...props} />}
      />
      <UserRoute
        location={location}
        path="/recipe/:recipe_id"
        exact
        component={(props) => <DisplayRecipePage {...props} />}
      />
      <UserRoute
        location={location}
        path="/recipe/:recipe_id"
        exact
        component={(props) => <DisplayRecipePage {...props} />}
      />
      <Route location={location} path="*" component={HomePage}></Route>
    </Switch>
  </div>
);

App.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token,
  };
}

export default connect(mapStateToProps)(App);
