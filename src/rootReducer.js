import { combineReducers } from "redux";

import user from "./reducers/user";
import recipe from "./reducers/recipes";

export default combineReducers({
  user,
  recipe
});
